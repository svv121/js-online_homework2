"use strict";
/*
1. Javascript має 6 примітивних типів даних: numeric, string, boolean, null, undefined і symbol. Object вважається окремим типом даних.
2. == - це оператор порівняння, який перед порівнянням перетворює операнди, наприклад, при порівнянні рядка із числом, JavaScript перетворює рядок у число; порожній рядок перетворюється в нуль; рядок без числового значення перетворюється в NaN (не число), що повертає false.
=== - це оператор суворого порівняння в JavaScript, якщо порівнювати число 3 та рядок "3" перетворювання типів не відбувається, повертається false.
3. Оператор - це спеціальний символ, що використовується для виконання операцій над операндами - значеннями та змінними.
*/
let userName;
do {
    userName = prompt("Please enter your name:", "");
} while (userName === null || userName.trim().length === 0);
let userAge;
do {
    userAge = +prompt("Please enter your age:", "");
} while (!isFinite(userAge) || userAge <= 0);
if (userAge < 18) {
    alert("You are not allowed to visit this website");
} else if (userAge < 22) {
    let ageBetween = confirm("Are you sure you want to continue?");
    if (ageBetween) {
        alert(`Welcome, ${userName}`);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert(`Welcome, ${userName}`);
}